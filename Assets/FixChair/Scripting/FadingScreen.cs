﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FadingScreen : MonoBehaviour
{
    Animator anim;

    public GameObject m_person;
    public GameObject m_personBone;
    public Animator m_personAnim;


    
    

    public float m_beforeFading;

   

    public CamMoveClose m_moveScript;

    void Awake()
    {
        anim = GetComponent<Animator>();
        StartAnim();
    }

    void HidePerson()
    {
        m_person.SetActive(false);
        m_personBone.SetActive(false);
        m_personAnim.enabled = false;
    }

    void MoveCamera()
    {
        m_moveScript.StartMoveClose();
    }

    void StartAnim()
    {
        StartCoroutine(Fading());
    }

    void ReplaceCanvas()
    {
        
        gameObject.SetActive(false);
       
    }

    IEnumerator Fading()
    {
        yield return new WaitForSeconds(m_beforeFading);
        anim.SetTrigger("Fading");
    }

    
}
