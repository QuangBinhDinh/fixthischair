﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class CamMoveToPaint : MonoBehaviour
{
    public float m_delay;
    public float m_duration, m_secondDuration;
    public Vector3[] m_waypoints;
    

    public Transform m_brokenPiece;

    public Transform m_dest;

    public Transform m_secondDest;

    public Transform m_localPaintCanPos;

    public GameObject m_paintCan;

    public GameObject m_paintCanvas;

    public Image hand,rotate;

    public FillCounter counter;

    public AudioSource source;

    CamToOrigin m_origin;
    
    public float m_halfThreshold, m_threshold;
    bool canRotate = false, passCounter = false;

    Camera cam;
    ParticleSystem particle;
    public ParticleSystem sparkle;


    Vector3 startWPos, endWPos, startLPos, endLPos;


    void Awake()
    {
        cam =GetComponent<Camera>();
        particle = m_paintCan.GetComponent<ParticleSystem>();
        m_origin = GetComponent<CamToOrigin>();
    }

    void Update()
    {
        if(counter.percent >= m_halfThreshold && !passCounter)
        {
            MoveToPaintNext();
            passCounter = true;
        }else if(counter.percent >= m_threshold)
        {
            m_origin.enabled = true;
            m_paintCan.SetActive(false);
            sparkle.Play();
            Debug.Log("completed");
            this.enabled = false;
        }
        if (!canRotate) return;
        if (Input.GetMouseButtonDown(0))
        {
           
            hand.enabled = false; rotate.enabled = false;
            particle.Play(); source.Play();
            startWPos = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.57f));
            startLPos = cam.transform.InverseTransformPoint(startWPos);
            
        }
        else if (Input.GetMouseButton(0))
        {
            endWPos = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.57f));
            endLPos = cam.transform.InverseTransformPoint(endWPos);
            m_paintCan.transform.localPosition += endLPos - startLPos;

            startLPos = endLPos;
          
            
        }
        else if (Input.GetMouseButtonUp(0))
        {
            particle.Stop();
            source.Stop();
        }



 
    }

    public void MoveCloseToPaint()
    {
        StartCoroutine(moveClose());
    }

    IEnumerator moveClose()
    {
        yield return new WaitForSeconds(m_delay);
       transform.DOMove(m_dest.position, m_duration);
       transform.DORotateQuaternion(m_dest.rotation, m_duration).SetEase(Ease.Linear).OnComplete(()=> {
           canRotate = true;
           m_paintCan.SetActive(true);
           m_paintCanvas.SetActive(true);
       });
    }

    public void MoveToPaintNext()
    {
        canRotate = false;
        m_paintCan.SetActive(false);
        transform.DOPath(m_waypoints, m_secondDuration,PathType.CatmullRom);
        transform.DORotateQuaternion(m_secondDest.rotation, m_secondDuration).OnComplete(()=> {
            
            m_paintCan.SetActive(true);
            m_paintCan.transform.localPosition = m_localPaintCanPos.position;
            m_paintCan.transform.localRotation = m_localPaintCanPos.rotation;
            canRotate = true;
        });
    }
}
