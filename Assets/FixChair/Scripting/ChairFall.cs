﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChairFall : MonoBehaviour
{

    Animator anim;
    AudioSource sound;

    public float m_delaySound;

    void Awake()
    {
        sound = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        StartCoroutine(playAnimFall());
    }

    IEnumerator playAnimFall()
    {
        yield return new WaitForSeconds(1f);

        anim.SetTrigger("Fall");
        

    }

    void PlaySound()
    {
        sound.enabled = true;
    }

   
   
}
