﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PaintIn3D.Examples;
using PaintIn3D;
using TMPro;

public class FillCounter : MonoBehaviour
{

    public List<P3dChangeCounter> Counters { get { if (counters == null) counters = new List<P3dChangeCounter>(); return counters; } }  [SerializeField] private List<P3dChangeCounter> counters;


    public int DecimalPlaces { set { decimalPlaces = value; } get { return decimalPlaces; } }[SerializeField] private int decimalPlaces;

    public TextMeshProUGUI m_text;

    public Image m_fill;

    public string Format { set { format = value; } get { return format; } }[Multiline] [SerializeField] private string format = "{PERCENT}";

    public float percent { get; private set; }

    public Animator m_textAnim;

    public CamMoveToPaint m_script;
    long initCount, total;

    

    void Awake()
    {
        var finalCounters = counters.Count > 0 ? counters : null;
        initCount = P3dChangeCounter.GetCount(finalCounters);
        total = P3dChangeCounter.GetTotal(finalCounters);
    }

    void Update()
    {

        if (percent >= m_script.m_threshold) return;
        
        var finalCounters = counters.Count > 0 ? counters : null;
        
        var count = P3dChangeCounter.GetCount(finalCounters);

        percent = P3dHelper.RatioToPercentage(P3dHelper.Divide(count - initCount, total - initCount), decimalPlaces);

        //Debug.Log(percent);
        //m_text.text = Mathf.Round(percent).ToString() + "%";

        //m_fill.fillAmount = percent / 100;

        
    }

   


}
