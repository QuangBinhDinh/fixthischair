﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BrokenPiece : MonoBehaviour
{
    public Transform m_pieceFrontPos;
    public float m_duration;

    public Vector3 m_rotateToFront;
    public float m_delay;

    public GameObject m_glue;
    public GameObject m_newPiece;
    public GameObject m_oldPiece;

    

    void Start()
    {
        
    }

    IEnumerator MoveToFrontCam()
    {
        yield return new WaitForSeconds(m_delay);
        Tween translate = transform.DOMove(m_pieceFrontPos.position, m_duration);
        Tween rotate = transform.DORotate(m_rotateToFront, m_duration).OnComplete(()=>Debug.Log("Rotated"));
        translate.OnComplete(()=>
        {
            m_glue.SetActive(true);
            m_newPiece.SetActive(true);
            m_oldPiece.SetActive(false);
            

        });
    }
    public void StartMoving()
    {
        StartCoroutine(MoveToFrontCam());
    }
}
