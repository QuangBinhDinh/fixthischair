﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SprayMovement : MonoBehaviour
{

    public float m_delay;

    public float m_duration;

    public Transform m_lookAt;

    ParticleSystem effect;

    private void Start()
    {
        
       effect = GetComponent<ParticleSystem>();
       
       StartCoroutine(PaintChair());
    }

    IEnumerator PaintChair()
    {
        yield return new WaitForSeconds(m_delay);
        transform.DOLookAt(m_lookAt.position, m_duration).OnComplete(()=> effect.Play());
        
        
    }

}
