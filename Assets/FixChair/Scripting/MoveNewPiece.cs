﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveNewPiece : MonoBehaviour
{
    public GameObject m_brokenChair;
    public GameObject m_fixedChair;

    public Vector3 m_rotateTo;
    public Transform m_repairPos;

    public float m_duration;
    public float m_delay;

    public float m_pushSpeed = 0.002f;

    public GameObject m_glueCanvas;

    bool readyToPush = false;

    string m_tagBroken = "BrokenChair";

    float endX, startX;


    void Update()
    {
        checkFixed();
        if (!readyToPush) return;

        if (Input.GetMouseButtonDown(0))
        {
            startX = Input.mousePosition.x;
            Destroy(m_glueCanvas);
        }
        if (Input.GetMouseButton(0))
        {

            endX = Input.mousePosition.x;

            float mulitiplier = (endX - startX) * m_pushSpeed;

            if (startX > endX && (transform.position.x <= m_repairPos.position.x || transform.position.z <= m_repairPos.position.z)) return;

            transform.position += transform.up * mulitiplier;

            startX = endX;

            
        }
    }

    IEnumerator MoveToFixPos()
    {
        yield return new WaitForSeconds(m_delay);
        Tween move = transform.DOMove(m_repairPos.position, m_duration).SetEase(Ease.Linear);
        
        Tween rotate = transform.DORotate(m_rotateTo, m_duration);
        move.OnComplete(()=> {
            readyToPush = true;
            m_glueCanvas.SetActive(true);
        });
    }

    // void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.transform.CompareTag(m_tagBroken))
    //    {
    //        Debug.Log("Fixed");
            
    //    }
    //}

    void checkFixed()
    {
        if(transform.position.x >= 2.816f && transform.position.z >= 1.2f && readyToPush)
        {
           
            readyToPush = false;
            Destroy(gameObject);
            Destroy(m_brokenChair);     
            m_fixedChair.SetActive(true);
            

        }
    }

    public void ReadyToFix()
    {
        StartCoroutine(MoveToFixPos());
    }


}
