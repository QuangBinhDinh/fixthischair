﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UsingGlue : MonoBehaviour
{
    public MoveNewPiece m_script;
    public GameObject m_gel;
    public Transform m_piece;

    public Transform m_coordinate;

    RectTransform rect;

    [Range(0,1)]
    public float m_sensitivity = 0.2f;

    public GameObject m_canvasGel;

    public Camera cam;

    public int glueCount;

    public Vector3 m_moveOutPos;

    AudioSource sound;

    GameObject temp;
    RaycastHit hit;

    Vector3 initPoint, endPoint;

    Vector3 initGelPos;

    Vector3 localInit;

    Vector3 mouseInit, mouseDest, mouseDelta;

    Animator glueAnim;

    bool glue = false, pressed = false;

    float timeGlue = 0f;

    string pieceTag = "Piece";
    string gelTag = "Gel";
    string blockTag = "NotPiece";


    void Awake()
    {
        initPoint = Vector3.zero;
        endPoint = Vector3.zero;
        glueAnim = GetComponent<Animator>();
        sound = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        if (glue && pressed) CreateGel();
        
    }

    private void Update()
    {
        if (m_piece.childCount > glueCount)
        {
            RemoveGlue();
            sound.Stop();
            m_script.ReadyToFix();
            this.enabled = false;
        }
        if (!glue) return;
        if (Input.GetMouseButtonDown(0))
        {
            pressed = true;
            m_canvasGel.SetActive(false);
            mouseInit = Input.mousePosition;
            localInit = m_coordinate.InverseTransformPoint(transform.position);
            sound.Play();
        }
        else if (Input.GetMouseButton(0))
        {
            mouseDest = Input.mousePosition;
            mouseDelta = mouseDest - mouseInit;

            Vector3 temp = new Vector3(mouseDelta.y, 0, -mouseDelta.x);

            localInit += temp / 100 * m_sensitivity;
            transform.position = m_coordinate.TransformPoint(localInit);

            mouseInit = mouseDest;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            sound.Stop();
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawLine(transform.position, Vector3.down * 15);
    }

    void CreateGel()
    {      
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 1f))
        {
            
            if (hit.transform.CompareTag(pieceTag) )
            {
                if (initPoint == Vector3.zero)
                {
                    initPoint = hit.point;
                    return;
                }
                endPoint = hit.point;

                float dist = Vector3.Magnitude(endPoint - initPoint);


                if (Mathf.Abs(dist)<= 0.003f) return;
                
                for(int i = 0; i< dist * 1000; i++)
                {
                    float k = i / (dist * 1000);
                    initGelPos = Vector3.Lerp(initPoint, endPoint, k);
                    temp = Instantiate(m_gel, m_piece);
                    temp.transform.position = initGelPos;                 
                }
                initPoint = endPoint;
                
            }
        }
    }

    void ToogleGel()
    {
        glue = !glue;
        glueAnim.enabled = false;
        
    }

    void SetCanvas()
    {
        m_canvasGel.SetActive(true);
    }


    void RemoveGlue()
    {
        transform.DOMove(m_moveOutPos, 0.75f).SetEase(Ease.OutQuad).OnComplete(() =>
        {
            gameObject.SetActive(false);

        });
        
    }
   

}
