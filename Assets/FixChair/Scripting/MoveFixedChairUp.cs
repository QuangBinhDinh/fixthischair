﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveFixedChairUp : MonoBehaviour
{
    public float m_delay;
    public float m_duration;


    public Transform m_destTransform;

    public CamMoveToPaint m_script;

    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(moveUp());
    }

    IEnumerator moveUp()
    {
        yield return new WaitForSeconds(m_delay);
        transform.DOMove(m_destTransform.position, m_duration).SetEase(Ease.Linear);
        transform.DORotateQuaternion(m_destTransform.rotation, m_duration).OnPlay(()=> m_script.MoveCloseToPaint());
    }
}
