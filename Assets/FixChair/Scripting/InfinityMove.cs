﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InfinityMove : MonoBehaviour
{
     RectTransform m_hand;
    public float m_duration;

    public Vector3[] m_wayPoint;

    void Start()
    {
        m_hand = GetComponent<RectTransform>();
       
        Tween move = m_hand.DOPath(m_wayPoint, m_duration, PathType.CatmullRom);
        move.SetAutoKill(false).SetLoops(-1);
    }

    
}
