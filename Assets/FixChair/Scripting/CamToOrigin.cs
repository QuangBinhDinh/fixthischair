﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CamToOrigin : MonoBehaviour
{
    public float m_delay, m_duration;

    public Transform m_dest;

    public GameObject m_paintCanvas;

    public GameObject confetti;

    AudioSource completed;


    void Start()
    {
        completed = GetComponent<AudioSource>();
        StartCoroutine(MoveToOriginal());
    }

    IEnumerator MoveToOriginal()
    {
        yield return new WaitForSeconds(m_delay);
        Destroy(m_paintCanvas);
        
        transform.DOMove(m_dest.position, m_duration).SetEase(Ease.OutQuad).OnComplete(()=> {
            confetti.SetActive(true);
            completed.Play();
        });
        transform.DORotateQuaternion(m_dest.rotation, m_duration);
    }
}
