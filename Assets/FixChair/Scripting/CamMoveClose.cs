﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CamMoveClose : MonoBehaviour
{

    Sequence camSeries;


    public Vector3[] waypoints;

    public Transform m_lookAtPos;

    public float m_moveDuration;

    public float m_delay;

    public BrokenPiece m_script;

    bool moving = false;

    Vector3 m_StartPos;
    void Start()
    {

        m_StartPos = transform.rotation.eulerAngles;
        StartMoveClose();
    }


    IEnumerator moveClose()
    {
        yield return new WaitForSeconds(m_delay);
        camSeries = DOTween.Sequence();
        Tween move = transform.DOPath(waypoints, m_moveDuration, PathType.CatmullRom).SetEase(Ease.OutQuad);
       
        Tween lookAt = transform.DORotateQuaternion(m_lookAtPos.rotation, m_moveDuration * 0.9f);
        

        camSeries.Append(move);
        camSeries.Insert(m_moveDuration * 0.1f , lookAt);

        camSeries.OnComplete(() => m_script.StartMoving());
        
    }

   

    public void StartMoveClose()
    {
        StartCoroutine(moveClose());
    }

    

    

    
}
