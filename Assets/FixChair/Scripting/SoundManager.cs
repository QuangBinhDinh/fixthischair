﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance { get; protected set; }

    public enum SoundType
    {
        Glue,WoodBreak,Paint
    }

   [System.Serializable]
   public class SoundAudioClip
    {
        public SoundType type;
        public AudioClip audio;
    }

    public  SoundAudioClip[] listSound;

    public  void playSound (SoundType type)
    {
        foreach(SoundAudioClip sound in listSound)
        {
            if(sound.type == type)
            {
                GameObject soundObj = new GameObject("Sound");
                AudioSource source = soundObj.AddComponent<AudioSource>();
                source.PlayOneShot(sound.audio);

            }
        }
    }
}
